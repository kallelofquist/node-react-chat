import socketIOClient from 'socket.io-client';

// Singleton
let SocketConnection = (function () {

    // Instance stores a reference to the Singleton
    var instance;
  
    function init() {
  
      const ENDPOINT = 'http://127.0.0.1:8080';
  
      return {
        socket: socketIOClient(ENDPOINT)
      };
  
    };
  
    return {
  
      // Get the Singleton instance if one exists
      // or create one if it doesn't
      getInstance: function () {
  
        if ( !instance ) {
          instance = init();
        }
  
        return instance;
      }
  
    };
  
  })();

  export default SocketConnection;