import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import Login from '../login/Login';
import Chat from '../chat/Chat';

function App() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Welcome!</h1>
        </header>
        <Router>
          <Switch>
            <Route exact path="/">
              <Login />
            </Route>
            <Route exact path="/chat/:username" component={Chat} />
            </Switch>
          </Router>
      </div>
    );
}

export default App;
