import React from 'react';

import './Chat.css';

import SocketConnection from '../utils/SocketConnection';
const socket = SocketConnection.getInstance().socket;

/*
*   Chat Component
*/
class Chat extends React.Component {

    constructor(props) {
        //Seems to be deprecated
        super(props);

        this.state = {
            users: [{
                "username": '',
                "id": ''
            }],
            token: '',
            decodedToken: {},
            messages: [{
                "message": '',
                "color": ''
            }],
            color: '#ffffff',
            newMessage: ''
        };

    }

    /*
    *   On mount of component
    */
    componentDidMount() {

        // Constant for URL to backend server
        const ENDPOINT = SocketConnection.ENDPOINT;

        socket.emit('username', this.props.match.params.username);

        // When the client gets a JWT back, set it into session storage and verify it
        socket.on('token', token => {
            if(sessionStorage.getItem('UserJWTToken') !== token) {
                sessionStorage.setItem("UserJWTToken", token);

                let myHeader = {
                    'Authorization': token
                };

                fetch(ENDPOINT + '/verify', {
                    headers: myHeader,
                    method: 'GET'
                })
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        decodedToken: data
                    }, () => {
                        socket.emit('newUser', this.state.decodedToken.username);
                    } );
                });
            }
            else if (!sessionStorage) {
                console.log('Your browser does not support session storage and therefore this app might not work correctly :(');
            }
        });

        // Update users array
        socket.on('newUserToList', users => {
            this.setState({
                users: users
            });
        });

        // Add a new message into array of messages
        socket.on('message', message => {
            this.setState({
                messages: [...this.state.messages, message]
            });

            // Keep the most recent message in view
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
        });

        // On disconnection, update the users array
        socket.on('disconnectedUser', updatedUsers => {
            this.setState({
                users: updatedUsers
            });
        });
    }

    /*
    *   Method that fires of when a user hits 'send'
    */
    sendMessage(e) {
        // If the input bar is not empty, send a new message with the users name, the message itself and the chosen color
        if(this.state.newMessage !== '') {
            socket.emit('newMessage', {message: `${this.state.decodedToken.username}: ${this.state.newMessage}`, color: this.state.color});

            this.setState({
                newMessage: ''
            });

            if(e) {
                e.target.focus();
            }
            else {
                document.getElementById('writeMessage').focus();
            }
        }
    }

    /*
    *   Updates the users chosen color from selected option in dropdown
    */
    updateColor(e) {
        this.setState({
            color: e.target.value
        });
    }

    render() {

        // Some lines to ensure that the user sees the correct users
        let usernameArray = []

        this.state.users.forEach(element => {
            usernameArray.push(element.username);
        });

		return (
			<div id="chat">
				<h1>Chat</h1>

                <div id="colorPicker">
                    <label htmlFor="colors">My color: </label>
                    <select name="colors" id="colors" onChange={(e) => this.updateColor(e)}>
                    <optgroup label="Colors">
                        <option value="white">White</option>
                        <option value="black">Black</option>
                        <option value="blue">Blue</option>
                        <option value="green">Green</option>
                        <option value="red">Red</option>
                        <option value="yellow">Yellow</option>
                    </optgroup>
                    </select>
                </div>

                <div id="chatSpace">

                    <div id="messages">
                        <ul>
                            {this.state.messages.map((message, index) =>
                                <li key={index} className="messageText" style={{color: message.color}}>{message.message}</li>
                            )}
                        </ul>
                    </div>

                    <div id="authorBox">
                        <div id="authoring">
                            <textarea id="writeMessage" cols="40" value={this.state.newMessage} onChange={(e) => this.setState({ newMessage: e.target.value })} onKeyDown={(e) => {if(e.keyCode === 13) {e.preventDefault(); this.sendMessage(e)}}}></textarea>
                            <button id="sendMessage" onClick={() => this.sendMessage()}>Send</button>
                        </div>
                    </div>

                    <h3>Active users:</h3>
                    <div id="userList">
                        <ul>
                            {usernameArray.map((user, index) =>
                                <li key={index} className="user"><img id="profilePic" src="https://thispersondoesnotexist.com/image" alt="profilePic"/>{user}</li>
                            )}
                        </ul>
                    </div>

                </div>
			</div>
		);
	}
}

export default Chat;