import React from 'react';
import './Login.css';

/*
*   Login Component
*/
class Login extends React.Component {

    constructor(props) {
        //Seems to be deprecated
        super(props);

        this.state = {
            username: ''
        }
    }

    // Method called when the user hits 'Login'. Sends the user on to the chat page with the username as a component parameter-
    login() {
       window.location.assign(window.location.href + `chat/${this.state.username}`);
    }

    render() {

		return (
			<div id="login">
				<h1>Login</h1>

				<div id="LoginBox">
                    <label htmlFor="loginInput">Username: </label>
					<input type="text" id="loginInput" value={this.state.username} onChange={e => this.setState({username: e.target.value})}/>
				</div>

				<div id="loginBtn">
                    <button id="btn" onClick={() => this.login()}>login</button>
				</div>
			</div>
		);
	}
}

export default Login;