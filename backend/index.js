/**
 * 	By: Karl Löfquist
 */

// Necessary constants
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);

const jwt = require('jsonwebtoken');

const fs = require('fs');
const publicKey = fs.readFileSync('./keys/public.pem', 'utf8');
const privateKey = fs.readFileSync('./keys/private.pem', 'utf8');

const cors = require('cors');

const socketio = require('socket.io');
const io = socketio(server);
let users = [];

const { PORT = 8080 } = process.env;

app.use(cors());

// Runs when a client connects
io.on('connection', (socket) => {

    console.log('Connect: ' + socket.id);

    socket.on('newUser', username => {

        console.log('newUser ' + socket.id);

        socket.emit('message', {message: `Welcome to this chat ${username}!`, color: 'white'});

        socket.broadcast.emit('message', {message: 'A user has joined the chat!', color: 'white'});

        let id = socket.id;
        users = [...users, {"username": username, "id": id}];
        console.log(...users);
        io.sockets.emit('newUserToList', users);
    });

    // Listen for login
    socket.on('username', username => {

        // Create user specific JWT
        let token = jwt.sign(
            {
              exp: Math.floor(Date.now() / 1000) + (60 * 60), // one hour
              "username": username,
            }, privateKey,
             {
                 algorithm: 'RS256'
             }
        );

        socket.emit('token', token);
    });

    socket.on('newMessage', message => {
        io.sockets.emit('message', message);
    });

    // Disconnecting
    socket.on('disconnect', (reason) => {

        if (reason === 'transport close') {
            console.log('Disconnect: ' + socket.id);
            let id = socket.id;
            let pos = users.map(function(e) { return e.id; }).indexOf(id);
        
            if(pos >= 0) {
                users.splice(pos, 1);
                socket.broadcast.emit('disconnectedUser', users);
                socket.broadcast.emit('message', {message: 'A user has left the chat', color: 'white'});
            }

            console.log(...users);
        }
    });
});

// Endpoint for verification of JWT ('/verify')
app.get('/verify', (req, res) => {

    let decodedJwt;

    try {
        decodedJwt = jwt.verify(req.headers.authorization, publicKey);
    } catch (err) {
        console.log(err);
    }

	res.send(decodedJwt);
});

server.listen(PORT, ()=> console.log(`Server started on port ${PORT}...`));
